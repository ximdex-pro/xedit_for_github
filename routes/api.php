<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1'], function () {
   
   // Authenticated user group
   Route::group(['middleware' => 'auth:api'], function () {
    
        // Git projects
        Route::post('create-project/{project}', 'GitController@create');
        Route::post('clone-project/{project}', 'GitController@clone');
        Route::post('remove-project/{project}', 'GitController@remove');
        Route::post('rename-project/{project}/{name}', 'GitController@rename');
        Route::get('project-status/{project}', 'GitController@status');
        Route::get('project-log/{project}', 'GitController@log');
        Route::get('project-branches/{project}', 'GitController@branches');
        Route::get('remote-branches/{project}', 'GitController@remote_branches');
        Route::get('current-branch/{project}', 'GitController@branch');
        Route::post('create-branch/{project}', 'GitController@new_branch');
        Route::post('remove-branch/{project}', 'GitController@delete_branch');
        Route::post('do-commit/{project}', 'GitController@commit');
        Route::post('discard-changes/{project}', 'GitController@discard');
        Route::post('checkout-branch/{project}', 'GitController@checkout');
        Route::post('reset-hard/{project}', 'GitController@reset');
        Route::post('do-push/{project}', 'GitController@push');
        Route::post('do-pull/{project}', 'GitController@pull');
        Route::post('add-files/{project}', 'GitController@add');
        Route::get('list-changes/{project}', 'GitController@changes');
        Route::post('merge-branch/{project}', 'GitController@merge');
        Route::post('remove-files/{project}', 'GitController@rm');
        Route::get('get-remote-url/{project}', 'GitController@get_url');
        Route::post('set-remote-url/{project}', 'GitController@set_url');
        
        // Files
        Route::post('read-file/{project}', 'FileController@read');
        Route::post('write-file/{project}', 'FileController@write');
        Route::post('delete-file/{project}', 'FileController@delete');
        Route::post('list-files/{project}', 'FileController@files');
        
        // Folders
        Route::post('make-folder/{project}', 'FolderController@make');
        Route::post('delete-folder/{project}', 'FolderController@delete');
        Route::post('list-folders/{project}', 'FolderController@folders');
        
        // User
        Route::post('user-logout', 'UserController@logout');
        
        // Xedit
        Route::group(['prefix' => 'xedit'], function () {
            Route::get('{project}', 'XeditController@recovery');
            Route::post('save/{project}', 'XeditController@save');
        });
    });
    
    // No user auth group
    Route::group(['middleware' => 'api'], function () {
            
        // User
        Route::post('user-login', 'UserController@login');
    });
});
