<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class UserController extends Controller
{
    use AuthenticatesUsers;
    
    /**
     * Check if email and password given is valid for an existant user, and generate a new token to save in user data
     * Return this token if everything works right
     * If remember_token is set to true, this token only will be generated if there is not an actual value
     * 
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {
            $this->validateLogin($request);
            if ($this->hasTooManyLoginAttempts($request)) {
                throw new \Exception('Too many attempts to login');
            }
            // $pass = Hash::make($request->input('password'));
            $user = User::whereEmail($request->input('email'))->first();
            if (!$user or !Hash::check($request->input('password'), $user->password)) {
                throw new \Exception('Wrong credentials or user not found');
            }
            if (!$user->api_token or $user->remember_token !== 'true' or $request->input('resetToken') === 'true') {
                $token = Password::broker()->createToken($user);
                if (!$token) {
                    throw new \Exception('Cannot generate a new token');
                }
                $user->api_token = $token;
                $user->update();
            }
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['token' => $user->api_token]);
    }
    
    /**
     * Revoke the current token for logged user
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            $user = Auth::user();
            $user->api_token = null;
            $user->update();
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
}
