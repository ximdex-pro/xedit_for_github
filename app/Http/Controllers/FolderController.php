<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FolderService;

class FolderController extends Controller
{
    /**
     * Create a folder by its path in a specific user project
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function make(Request $request, string $project)
    {
        try {
            FolderService::create($project, $request->input('folder'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
    
    /**
     * Delete a folder by its path in a specific user project
     *
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, string $project)
    {
        try {
            FolderService::remove($project, $request->input('folder'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
    
    /**
     * Retrive a list of subfolder names for a given directory path in one project
     * If folder path input parameter is not present, it return the subfolders from project root
     *
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function folders(Request $request, string $project)
    {
        try {
            $folders = FolderService::list($project, $request->input('folder'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['folders' => $folders]);
    }
}
