<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\GitService;

class GitController extends Controller
{
    /**
     * Create a new git local repository
     * 
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(string $project)
    {
        try {
            GitService::create($project);
            generate_project_structure($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
    
    /**
     * Clone a git repository with an remote source given
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function clone(Request $request, string $project)
    {
        try {
            GitService::clone($project, $request->input('url'), $request->input('user'), $request->input('token'));
            generate_project_structure($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
    
    /**
     * Delete a project folder
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove(Request $request, string $project)
    {
        try {
            if (!$request->input('project-name') or $request->input('project-name') != $project) {
                throw new \Exception('Project name is not valid (Remember to send this name in request data)');
            }
            GitService::destroy($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
    
    /**
     * Rename a project
     * 
     * @param string $project
     * @param string $name
     * @return \Illuminate\Http\JsonResponse
     */
    public function rename(string $project, string $name)
    {
        try {
            GitService::rename($project, $name);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
    
    /**
     * Get user project status information
     * 
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(string $project)
    {
        try {
            $status = GitService::status($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['status' => $status]);
    }
    
    /**
     * Retrieve the user project log information
     * 
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function log(string $project)
    {
        try {
            $log = GitService::log($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['log' => $log]);
    }
    
    /**
     * Do a commit in the project
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function commit(Request $request, string $project)
    {
        try {
            $status = GitService::commit($project, $request->input('message'), $request->input('user'), $request->input('email'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['status' => $status]);
    }
    
    /**
     * Return a list of project branches
     * 
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function branches(string $project)
    {
        try {
            $branches = GitService::branches($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['branches' => $branches]);
    }
    
    /**
     * Return a list of remote branches
     *
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function remote_branches(string $project)
    {
        try {
            $branches = GitService::remote_branches($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['branches' => $branches]);
    }
    
    /**
     * Retrieve the current project branch
     * 
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function branch(string $project)
    {
        try {
            $branch = GitService::branch($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['branch' => $branch]);
    }
    
    /**
     * Create a new branch by given name
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function new_branch(Request $request, string $project)
    {
        try {
            $result = GitService::create_branch($project, $request->input('branch'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['message' => $result]);
    }
    
    /**
     * Delete a branch by given name
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_branch(Request $request, string $project)
    {
        try {
            $result = GitService::delete_branch($project, $request->input('branch'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['message' => $result]);
    }
    
    /**
     * Checkout to a specified branch given, or the active if any instead
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkout(Request $request, string $project)
    {
        try {
            if (!$request->input('branch')) {
                $branch = GitService::branch($project);
                if (!$branch) {
                    throw new \Exception('No branch name to checkout defined');
                }
            } else {
                $branch = $request->input('branch');
            }
            $result = GitService::checkout($project, $branch);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['message' => $result]);
    }
    
    /**
     * Make a push to the current active branch with the remote user and password (or token) given
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function push(Request $request, string $project)
    {
        try {
            $result = GitService::push($project, $request->input('user'), $request->input('token'), $request->input('force'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['message' => $result]);
    }
    
    /**
     * Make a pull from the current active branch
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function pull(Request $request, string $project)
    {
        try {
            $result = GitService::pull($project, $request->input('user'), $request->input('token'), (bool) $request->input('force'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['message' => $result]);
    }
    
    /**
     * Add all changed or created files to the changes list to be committed 
     * 
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(string $project)
    {
        try {
            GitService::add($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
    
    /**
     * Remove all files in order to be committed
     *
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function rm(string $project)
    {
        try {
            GitService::rm($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
    
    /**
     * Get the remote URL
     * 
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_url(string $project)
    {
        try {            
            $url = GitService::get_url($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['url' => $url]);
    }
    
    /**
     * Change the remote URL to a new one where push commits
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function set_url(Request $request, string $project)
    {
        try {
            if (!$request->input('url')) {
                throw new \Exception('Parameter URL not defined');
            }
            GitService::set_url($project, $request->input('url'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
    
    /**
     * Discard local changes for a specified file
     * 
     * @param Request $request
     * @param string $project
     * @throws \Exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function discard(Request $request, string $project)
    {
        try {
            if (!$request->input('file')) {
                throw new \Exception('File name to discard is not defined');
            }
            GitService::discard($project, $request->input('file'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
    
    /**
     * Retrieve a list of files ready to be committed
     * 
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function changes(string $project)
    {
        try {
            GitService::add($project);
            $files = GitService::changes($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['files' => $files]);
    }
    
    /**
     * Merge changes in the project
     * Branch input value is optional
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function merge(Request $request, string $project)
    {
        try {
            $result = GitService::merge($project, $request->input('branch'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['message' => $result]);
    }
    
    /**
     * Make a reset to previous commit
     * 
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(string $project)
    {
        try {
            $status = GitService::reset($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['status' => $status]);
    }
}
