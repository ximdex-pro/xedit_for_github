<?php

namespace App\Http\Controllers;

use App\Models\Xedit;
use Xedit\Base\Models\Mapper;
use Xedit\Base\Models\DocumentMapper;
use Illuminate\Http\Request;

class XeditController extends Controller
{
    public function __construct()
    {
        $this->xedit_model = Xedit::class;
        /*
        $mapper = new RouterMapper('');
        $mapper->setSaveUrl($_GET['project']."/save/:id");
        */
    }
    
    public function mapper(Request $request)
    {
        $mapper = new Mapper(new DocumentMapper($request->get('project')), $this->router, $this->data);
        return [
            'message' => '',
            'status' => 0,
            'response' => $mapper->toArray()
        ];
    }

    public function recovery(Request $request, string ...$args)
    {
        if (!isset($args[0]) or !$args[0]) {
            throw new \Exception('No data for project name');
        }
        if (!$request->input('resource')) {
            throw new \Exception('No data for resource name');
        }
        // $section = ($this->xedit_model)::get($id);
        $section = ($this->xedit_model)::get($args[0], $request->input('resource'));
        $result = \Xedit\Base\Core\Xedit::getContentByNode($section);
        return [
            'message' => '',
            'status' => 0,
            'response' => [
                "nodes" => $result
            ]
        ];
    }
    
    public function save(Request $request, string ...$args)
    {
        $data = $request->json()->all();
        $container = ($this->xedit_model)::get($args[0], $request->input('resource'))->getContainer();
        $content = $data['nodes']["xe_{$container->getId()}"];
        $content = isset($content['content']) ? $content['content'] : '';
        try {
            $container->saveContent($content);
            $result = [
                'message' => '',
                'response' => 'Saved',
                'status' => 0
            ];
        } catch (\Exception $e) {
            $result = [
                'message' => $e->getMessage(),
                'response' => 'Error',
                'status' => 0
            ];
        }
        return $result;
    }
}
