<?php

namespace App\Http\Controllers;

use App\Services\FileService;
use Illuminate\Http\Request;
use App\Services\GitService;

class FileController extends Controller
{
    /**
     * Read the content of a given file path in a specific user project
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request, string $project)
    {
        try {
            $content = FileService::read($project, $request->input('file'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['content' => $content]);
    }
    
    /**
     * Write the content of a given file path in a specific user project
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function write(Request $request, string $project)
    {
        try {
            FileService::write($project, $request->input('file'), (string) $request->input('content'));
            GitService::add($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
    
    /**
     * Delete a given file by its path in a specific user project
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, string $project)
    {
        try {
            FileService::delete($project, $request->input('file'));
            GitService::add($project);
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response();
    }
    
    /**
     * Retrive a list of file names for a given directory path in one project
     * If folder path input parameter is not present, it return the files from project root
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function files(Request $request, string $project)
    {
        try {
            $files = FileService::files($project, $request->input('folder'));
        } catch (\Exception $e) {
            return json_error_response($e->getMessage());
        }
        return json_success_response(['files' => $files]);
    }
}
