<?php

namespace App\Models;

use App\Services\FileService;
use Xedit\Base\Interfaces\Models\IXeditComponent;
use Xedit\Base\Interfaces\Models\IXeditContainer;
use Xedit\Base\Interfaces\Models\IXeditLayout;

class XeditLayout implements IXeditLayout
{
    private $id;
    private $content;
    public static $project;
    
    public function __construct(string $project, string $resource)
    {
        if (!$project or !$resource) {
            throw new \Exception('Layout cannot be loaded without a project and resource names');
        }
        project_exists($project);
        self::$project = $project;
        $content = FileService::read(self::$project, 'layouts/' . $resource . '.json');
        $data = json_decode($content);
        if ($data === null) {
            throw new \Exception('Layout ' . $resource . ' has not a valid JSON code');
        }
        $this->id = $resource;
        $this->content = $content;
    }
    
    public function getContent(): string
    {
        return $this->content;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getComponent($condition): IXeditComponent
    {
        return new XeditComponent(self::$project, $condition);
    }

    public function getIncludeContainer($condition): IXeditContainer
    {
        return new XeditContainer(self::$project, $condition);
    }
}
