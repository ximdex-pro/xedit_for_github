<?php

namespace App\Models;

use App\Services\FileService;
use Xedit\Base\Interfaces\Models\IXeditView;

class XeditView implements IXeditView
{
    private $id;
    private $content;
    
    public function __construct(string $project, string $resource)
    {
        if (!$project or !$resource) {
            throw new \Exception('View cannot be loaded without a project or resource');
        }
        project_exists($project);
        $content = FileService::read($project, 'views/' . $resource . '.html');
        $this->id = $resource;
        $this->content = $content;
    }
    
    public function getContent(): string
    {
        return $this->content;
    }

    public function getId()
    {
        return $this->id;
    }
}
