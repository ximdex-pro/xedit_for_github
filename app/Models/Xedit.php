<?php

namespace App\Models;

use Xedit\Base\Interfaces\Models\IXedit;
use Xedit\Base\Interfaces\Models\IXeditContainer;

class Xedit implements IXedit
{
    private $id;
    private $container;
    
    public static function get(string $project, string $resource): IXedit
    {
        if (!$project or !$resource) {
            throw new \Exception('Cannot load a container without any resource or project name');
        }
        project_exists($project);
        $model = new static();
        $model->id = $resource;
        $model->container = new XeditContainer($project, $resource);
        return $model;
    }
    
    public function getContainer(): IXeditContainer
    {
        return $this->container;
    }

    public function getId()
    {
        return $this->id;
    }
}
