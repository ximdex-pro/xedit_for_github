<?php

namespace App\Models;

use App\Services\FileService;
use Xedit\Base\Interfaces\Models\IXeditContainer;
use Xedit\Base\Interfaces\Models\IXeditLayout;

class XeditContainer implements IXeditContainer
{
    private $id;
    private $layout;
    private $title;
    private $content;
    public static $project;
    
    public function __construct(string $project, string $resource)
    {
        if (!$project or !$resource) {
            throw new \Exception('Container cannot be loaded without a project and resource names');
        }
        project_exists($project);
        $data = FileService::read($project, 'containers/' . $resource . '.json');
        $data = json_decode($data);
        if ($data === null) {
            throw new \Exception('Container ' . $resource . ' has not a valid JSON code');
        }
        if (!isset($data->layout_id)) {
            throw new \Exception('Container ' . $resource . ' does not specify any layout');
        }
        $this->id = $resource;
        $this->title = file_name($resource);
        if (isset($data->content)) {
            $this->content = $data->content;
        } else {
            $this->content = '';
        }
        $this->layout = new XeditLayout($project, $data->layout_id);
        self::$project = $project;
    }
    
    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getId()
    {
        return $this->id;
    }

    public function saveContent($content): bool
    {
        // TODO
        
        return true;
    }
    
    public function getLayout(): IXeditLayout
    {
        return $this->layout;
    }
}
