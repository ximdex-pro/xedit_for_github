<?php

namespace App\Models;

use App\Services\FileService;
use Xedit\Base\Interfaces\Models\IXeditComponent;
use Xedit\Base\Interfaces\Models\IXeditView;

class XeditComponent implements IXeditComponent
{
    private $id;
    private $content;
    private $view;
    
    public function __construct(string $project, string $resource)
    {
        if (!$project or !$resource) {
            throw new \Exception('Component cannot be loaded without a project or resource');
        }
        project_exists($project);
        $content = FileService::read($project, 'components/' . $resource . '.json');
        $data = json_decode($content);
        if ($data === null) {
            throw new \Exception('Component ' . $resource . ' has not a valid JSON code');
        }
        if (!isset($data->schema->view)) {
            throw new \Exception('Component ' . $resource . ' does not specify a schema or view');
        }
        $this->id = $resource;
        $this->content = $content;
        $this->view = new XeditView($project, $data->schema->view);
    }
    
    public function getContent(): string
    {
        return $this->content;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getView(): IXeditView
    {
        return $this->view;
    }
}
