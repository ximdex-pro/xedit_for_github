<?php

namespace App\Core;

use ReflectionClass;

class Enum
{
    private static $constCacheArray;
    protected $current_constant;

    public function __construct($constant)
    {
        if (!self::isValidName($constant)) {
            throw new \InvalidArgumentException('Invalid constant name');
        }
        $this->current_constant = $constant;
    }

    /**
     * Get all constants in the class
     * @return array
     * @throws null
     */
    public static function getConstants()
    {
        if (self::$constCacheArray == NULL) {
            self::$constCacheArray = [];
        }
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    /**
     * Check if enum name exist in the class
     * @param string $name Name of enum value
     * @param bool $strict If it is false, it ignore case sensitive.
     * @return bool
     */
    public static function isValidName($name, $strict = false)
    {
        $constants = self::getConstants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    /**
     * Check if enum value exist in the class
     *
     * @param mixed $value Value of enum
     * @param bool $strict If it is false, it ignore the type.
     * @return bool
     */
    public static function isValidValue($value, $strict = true)
    {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict);
    }
}
