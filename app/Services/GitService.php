<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class GitService
{
    private static $repo;
    
    /**
     * Create a new Git project
     * 
     * @param string $repo
     * @throws \Exception
     */
    public static function create(string $project)
    {
        try {
            $projectPath = self::create_project_path($project);
            self::$repo = \Git::create(Storage::path($projectPath));
        } catch (\Exception $e) {
            if (isset($projectPath) and Storage::exists($projectPath)) {
                Storage::deleteDirectory($projectPath);
            }
            throw new \Exception($e->getMessage());
        }
    }
    
    /**
     * Clonate a remote git repository by given URL
     * User and token parameters are necessary for private projects
     * 
     * @param string $project
     * @param string $url
     * @param string $user
     * @param string $token
     * @throws \Exception
     */
    public static function clone(string $project, string $url, string $user = null, string $token = null)
    {
        try {
            if (!$url) {
                throw new \Exception('Parameter URL not defined');
            }
            if (filter_var($url, FILTER_VALIDATE_URL) === false) {
                throw new \Exception($url . ' is not a valid URL address');
            }
            if ($user) {
                if (!$token) {
                    throw new \Exception('No password has been sent for user name ' . $user);
                }
                $url = add_credentials_to_url($url, $user, $token);
            }
            $projectPath = self::create_project_path($project);
            self::$repo = \Git::clone_remote(Storage::path($projectPath), $url);
        } catch (\Exception $e) {
            if (isset($projectPath) and Storage::exists($projectPath)) {
                Storage::deleteDirectory($projectPath);
            }
            throw new \Exception($e->getMessage());
        }
    }
    
    /**
     * Delete a specified Git project from user projects 
     * 
     * @param string $project
     */
    public static function destroy(string $project)
    {
        $projectPath = project_exists($project);
        
        // Remove project folder
        Storage::deleteDirectory($projectPath);
        
        // Remove user folder for projects if there is not any project more
        $projectsPath = projects_path();
        $projects = Storage::directories($projectsPath);
        if (!$projects) {
            Storage::deleteDirectory($projectsPath);
        }
    }
    
    /**
     * Rename a repository project folder
     * 
     * @param string $project
     * @param string $name
     * @throws \Exception
     */
    public static function rename(string $project, string $name)
    {
        if (!$project or !$name) {
            throw new \Exception('Current project name and new one not specified');
        }
        if ($project == $name) {
            throw new \Exception('New project name is the same that the actual one');
        }
        project_exists($project);
        project_not_exists($name);
        File::moveDirectory(Storage::path(projects_path($project)), Storage::path(projects_path($name)));
    }
    
    /**
     * Get repository status information
     * 
     * @param string $project
     * @return string
     */
    public static function status(string $project) : string
    {
        self::select($project);
        $status = self::$repo->status();
        return trim($status);
    }
    
    /**
     * Retrieve the repository log information
     * 
     * @param string $project
     * @return string
     */
    public static function log(string $project) : string
    {
        self::select($project);
        $log = self::$repo->log();
        return trim($log);
    }
    
    /**
     * Retrieve a list of files ready to be committed
     * 
     * @param string $project
     * @return array
     */
    public static function changes(string $project) : array
    {
        self::select($project);
        $files = self::$repo->run('diff HEAD --name-only');
        if ($files) {
            $files = explode(PHP_EOL, trim($files));
        } else {
            $files = [];
        }
        return $files;
    }
    
    /**
     * Add all untracked files to local git track list
     * 
     * @param string $project
     */
    public static function add(string $project)
    {
        self::select($project);
        self::$repo->add('--all');
    }
    
    /**
     * Do a commit in the repository
     * 
     * @param string $project
     * @param string $message
     * @param string $user
     * @param string $email
     * @throws \Exception
     * @return string
     */
    public static function commit(string $project, string $message, string $user = null, string $email = null)
    {
        self::select($project);
        if (!$message) {
            throw new \Exception('Message for commit is not defined');
        }
        if (!self::changes($project)) {
            throw new \Exception('There is not changes to be committed');
        }
        if ($user and $email) {
            self::$repo->run('config user.email "' . $email . '"');
            self::$repo->run('config user.name "' . $user . '"');
        } else {
            $email = Auth::user()->email;
            $user = Auth::user()->name;
        }
        // $status = self::$repo->commit($request->get('message'));
        $command = 'commit -v --author "' . $user . ' <' . $email . '>" -m ' . escapeshellarg($message);
        $status = self::$repo->run($command);
        return trim($status);
    }
    
    /**
     * Make a push to the current active branch with the remote user and password (or token) given
     * 
     * @param string $project
     * @param string $user
     * @param string $token
     * @param string $force
     * @throws \Exception
     * @return string
     */
    public static function push(string $project, string $user, string $token, string $force = null) : string
    {
        if (!$user or !$token) {
            throw new \Exception('No user name or token have been sent');
        }
        self::select($project);
        $url = add_credentials_to_url(self::get_remote_url(), $user, $token);
        $command = 'push --all -u';
        if ($force == 'true') {
            $command .= ' --force';
        }
        $command .= ' ' . $url;
        // $result = self::$repo->push();
        $result = self::$repo->run($command);
        return trim($result);
    }
    
    /**
     * Make a pull from the current active branch
     * 
     * @param string $project
     * @param string $user
     * @param string $token
     * @param bool $force
     * @return string
     */
    public static function pull(string $project, string $user = null, string $token = null, bool $force = false) : string
    {
        self::select($project);
        if ($user and $token) {
            $url = add_credentials_to_url(self::get_remote_url(), $user, $token);
            $command = 'pull ' . $url;
            if ($force) {
                $command .= ' --allow-unrelated-histories';
            }
            $result = self::$repo->run($command);
        } else {
            $result = self::$repo->pull();
        }
        return trim($result);
    }
    
    /**
     * Return a list of project branches
     * 
     * @param string $project
     * @return array
     */
    public static function branches(string $project) : array
    {
        self::select($project);
        return self::$repo->list_branches();
    }
    
    /**
     * Return a list of remote branches
     * 
     * @param string $project
     * @return array
     */
    public static function remote_branches(string $project) : array
    {
        self::select($project);
        return self::$repo->list_remote_branches();
    }
    
    /**
     * Retrieve the current active branch
     * 
     * @param string $project
     * @return string
     */
    public static function branch(string $project) : string
    {
        self::select($project);
        return self::$repo->active_branch();
    }
    
    /**
     * Create a new branch
     * 
     * @param string $project
     * @param string $branch
     * @throws \Exception
     * @return string
     */
    public static function create_branch(string $project, string $branch) : string
    {
        if (!$branch) {
            throw new \Exception('No branch name to create defined');
        }
        self::select($project);
        $result = self::$repo->create_branch($branch);
        return trim($result);
    }
    
    /**
     * Delete a branch
     * 
     * @param string $project
     * @param string $branch
     * @throws \Exception
     * @return string
     */
    public static function delete_branch(string $project, string $branch) : string
    {
        if (!$branch) {
            throw new \Exception('No branch name to delete defined');
        }
        self::select($project);
        $result = self::$repo->delete_branch($branch, true);
        return trim($result);
    }
    
    /**
     * Checkout to a specified branch
     */
    public static function checkout(string $project, string $branch) : string
    {
        if (!$branch) {
            throw new \Exception('No branch name to checkout defined');
        }
        self::select($project);
        $result = self::$repo->checkout($branch);
        return trim($result);
    }
    
    /**
     * Remove all tracked files from local git track list
     * 
     * @param string $project
     */
    public static function rm(string $project)
    {
        self::select($project);
        self::$repo->rm();
    }
    
    /**
     * Discard local changes for a specified file
     * 
     * @param string $project
     * @param string $file
     */
    public static function discard(string $project, string $file)
    {
        if (!$file) {
            throw new \Exception('File name is not defined');
        }
        self::select($project);
        validate_file($project, $file);
        self::$repo->run('checkout -- ' . $file);
    }
    
    /**
     * Get the remote URL
     * 
     * @param string $project
     * @return string
     */
    public static function get_url(string $project) : string
    {
        self::select($project);
        return self::get_remote_url();
    }
    
    /**
     * Change the remote URL to a new one where push commits
     * 
     * @param string $project
     * @param string $url
     * @throws \Exception
     */
    public static function set_url(string $project, string $url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new \Exception($url . ' is not a valid URL address');
        }
        self::select($project);
        self::$repo->run('remote set-url origin ' . $url);
    }
    
    /**
     * Merge changes in the project
     * Branch input value is optional
     * 
     * @param string $project
     * @param string $branch
     * @return string
     */
    public static function merge(string $project, string $branch = null) : string
    {
        self::select($project);
        $result = self::$repo->merge($request->input('branch'));
        return trim($result);
    }
    
    /**
     * Make a reset to previous commit
     * 
     * @param string $project
     * @return string
     */
    public static function reset(string $project) : string
    {
        self::select($project);
        $result = self::$repo->run('reset --hard HEAD~');
        return trim($result);
    }
    
    /**
     * Create the project path for the user and proyect name given
     * Return the path to new project
     *
     * @param string $project
     * @throws \Exception
     * @return string
     */
    private static function create_project_path(string $project) : string
    {
        project_not_exists($project);
        $projectPath = projects_path();
        if (!Storage::exists($projectPath)) {
            
            // Create the user projects folder
            Storage::makeDirectory($projectPath);
        }
        return projects_path($project);
    }
    
    /**
     * Open a git project resource with a given name
     * 
     * @param string $project
     */
    private static function select(string $project)
    {
        $projectPath = project_exists($project);
        self::$repo = \Git::open(Storage::path($projectPath));
    }
    
    /**
     * Get the remote url to push if the repository has one setted
     *
     * @throws \Exception
     * @return string
     */
    private static function get_remote_url() : string
    {
        if (!self::$repo or !\Git::is_repo(self::$repo)) {
            throw new \Exception('No repository selected');
        }
        return trim(self::$repo->run('remote get-url --push origin'));
    }
}
