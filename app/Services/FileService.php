<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class FileService
{
    /**
     * Read file content from a specified user project
     * 
     * @param string $project
     * @param string $file
     * @return string
     */
    public static function read(string $project, string $file) : string
    {
        $projectPath = project_exists($project);
        validate_file($project, $file, true);
        return Storage::get($projectPath . '/' . $file);
    }
    
    /**
     * Write a text content to a file in a user project
     * 
     * @param string $project
     * @param string $file
     * @param string $content
     */
    public static function write(string $project, string $file, string $content)
    {
        $projectPath = project_exists($project);
        validate_file($project, $file);
        Storage::put($projectPath . '/' . $file, $content);
    }
    
    /**
     * Delete a file in the user project
     * 
     * @param string $project
     * @param string $file
     */
    public static function delete(string $project, string $file)
    {
        $projectPath = project_exists($project);
        validate_file($project, $file, true);
        Storage::delete($projectPath . '/' . $file);
    }
    
    public static function files(string $project, string $folder = null)
    {
        $path = project_exists($project);
        if ($folder) {
            validate_folder($project, $folder, true);
            $path .= '/' . $folder;
        }
        /*
         $files = Storage::files($projectPath);
         foreach ($files as & $file) {
         $file = basename($file);
         }
         */
        $path = Storage::path($path);
        $files = [];
        foreach (scandir($path) as & $file) {
            if (is_dir($path . '/' . $file)) {
                continue;
            }
            $files[] = $file;
        }
        return $files;
    }
}
