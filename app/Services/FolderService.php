<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class FolderService
{
    /**
     * Create a new directory in a specified user project
     * 
     * @param string $project
     * @param string $folder
     */
    public static function create(string $project, string $folder)
    {
        $projectPath = project_exists($project);
        validate_folder($project, $folder, false);
        Storage::makeDirectory($projectPath . '/' . $folder);
    }
    
    /**
     * Delete a directory in the given project
     * 
     * @param string $project
     * @param string $folder
     */
    public static function remove(string $project, string $folder)
    {
        $projectPath = project_exists($project);
        validate_folder($project, $folder, true);
        Storage::deleteDirectory($projectPath . '/' . $folder);
    }
    
    /**
     * Retrieve a list of subfolders for a given folder or the project root ones instead
     * 
     * @param string $project
     * @param string $folder
     * @return array
     */
    public static function list(string $project, string $folder = null) : array
    {
        $path = project_exists($project);
        if ($folder) {
            validate_folder($project, $folder, true);
            $path .= '/' . $request->input('folder');
        }
        $folders = [];
        /*
        foreach (Storage::directories($path) as $folder) {
            $folder = basename($folder);
            if ($folder == '.git') {
                continue;
            }
            $subfolders[] = $folder;
         }
         */
        $path = Storage::path($path);
        foreach (scandir($path) as & $folder) {
            if (!is_dir($path . '/' . $folder) or $folder == '.' or $folder == '..' or $folder == '.git') {
                continue;
            }
            $folders[] = $folder;
        }
        return $folders;
    }
}
