<?php

namespace App\Services;

class XeditService
{
    /**
     * Return the project name from a path condition
     * 
     * @param string $condition
     * @return string
     */
    public static function project_name(string $condition) : string
    {
        $project = trim(dirname($condition), './');
        if (!$project) {
            throw new \Exception('Condition parameter has not a valid project name');
        }
        project_exists($project);
        return $project;
    }
    
    /**
     * Return the resource in a condition (avoid project name)
     * 
     * @param string $project
     * @param string $condition
     * @return string
     */
    public static function resource_name(string $project, string $condition) : string
    {
        project_exists($project);
        return trim(preg_replace('/' . $project . '/', '', $condition, 1), '/');
    }
}
