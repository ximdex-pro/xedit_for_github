<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

// Path to users projects under Laravel app storage
const PROJECTS_PATH = 'projects';

/**
 * Return a JSON response with error message
 *
 * @param string $error
 * @return \Illuminate\Http\JsonResponse
 */
function json_error_response(string $error) : Illuminate\Http\JsonResponse
{
    return response()->json(['result' => 'error', 'message' => trim($error)]);
}

/**
 * Return a JSON response with success message and optional information
 *
 * @param array $params
 * @return \Illuminate\Http\JsonResponse
 */
function json_success_response(array $params = []) : Illuminate\Http\JsonResponse
{
    return response()->json(['result' => 'success'] + $params);
}

/**
 * Retrieves the URL given with the user and token given
 * 
 * @param string $url
 * @param string $user
 * @param string $token
 * @throws Exception
 * @return string
 */
function add_credentials_to_url(string $url, string $user = null, string $token) : string
{
    if (filter_var($url, FILTER_VALIDATE_URL) === false) {
        throw new Exception('Resource ' . $url . ' is not a valid URL');
    }
    if (!$user or !$token) {
        return $this->error('No user name or token has been given');
    }
    $components = parse_url($url);
    $result = $components['scheme'] . '://' . $user . ':' . $token . '@' . $components['host'];
    if (isset($components['port'])) {
        $result .= ':' . $components['port'];
    }
    $result .= $components['path'];
    if (isset($components['query'])) {
        $result .= '?' . $components['query'];
    }
    return $result;
}

/**
 * Return the projects path for the current logged user
 * If project argument is defined, the path will contain the full route to this project name
 * 
 * @param string $project
 * @throws Exception
 * @return string
 */
function projects_path(string $project = null) : string
{
    if (!Auth::user() or !Auth::user()->name) {
        throw new Exception('User name is needed');
    }
    return PROJECTS_PATH . '/' . Auth::user()->name . ($project ? '/' . $project : '');
}

/**
 * Validate a file in the user project given
 * 
 * @param string $project
 * @param string $file
 * @param bool $mustExists
 * @throws Exception
 */
function validate_file(string $project, string $file, bool $mustExists = null)
{
    if (!$project or !$file) {
        throw new Exception('No project or file parameter defined');
    }
    if (strpos($file, '.git/') === 0 or strpos($file, '..') !== false) {
        throw new Exception('Cannot read / write resource ' . $file);
    }
    $path = projects_path($project) . '/' . $file;
    if (File::isDirectory(Storage::path($path))) {
        throw new Exception('Resource ' . $file . ' is a folder');
    }
    if ($mustExists !== null) {
        $isCreated = Storage::exists($path);
        if ($mustExists and !$isCreated) {
            throw new Exception('File ' . $file . ' does not exist in project ' . $project);
        } elseif (!$mustExists and $isCreated) {
            throw new Exception('File ' . $file . ' already exist in project ' . $project);
        }
    }
}

/**
 * Validate a folder in the user project given
 * 
 * @param string $project
 * @param string $folder
 * @param bool $mustExists
 * @throws Exception
 */
function validate_folder(string $project, string $folder, bool $mustExists = null)
{
    if (!$project or !$folder) {
        throw new Exception('No project or folder parameter defined');
    }
    $folder = trim($folder, '/');
    if (strpos($folder, '.git') === 0 or strpos($folder, '..') !== false) {
        throw new Exception('Cannot read or write resource ' . $folder);
    }
    $path = projects_path($project) . '/' . $folder;
    if (File::isFile(Storage::path($path))) {
        throw new Exception('Resource ' . $folder . ' is a file');
    }
    if ($mustExists !== null) {
        $isCreated = Storage::exists($path);
        if ($mustExists and !$isCreated) {
            throw new Exception('Folder ' . $folder . ' does not exists in project ' . $project);
        } elseif (!$mustExists and $isCreated) {
            throw new Exception('Folder ' . $folder . ' already exists in project ' . $project);
        }
    }
}

/**
 * Return the project path if the given user project exists
 * 
 * @param string $project
 * @throws \Exception
 * @return string
 */
function project_exists(string $project) : string
{
    $projectPath = projects_path($project);
    if (!Storage::exists($projectPath)) {
        throw new Exception('Project ' . $project . ' is not created');
    }
    return $projectPath;
}

/**
 * Raise an exception if the given user project exists
 *
 * @param string $project
 * @throws \Exception
 */
function project_not_exists(string $project)
{
    if (Storage::exists(projects_path($project))) {
        throw new Exception('There is a project with the name ' . $project . ' created');
    }
}

/**
 * Return the file name from a given path without extension if any
 * 
 * @param string $path
 * @throws Exception
 * @return string
 */
function file_name(string $path) : string
{
    $parts = pathinfo($path);
    if (!isset($parts['filename'])) {
        throw new Exception('No file name found in the given path');
    }
    return $parts['filename'];
}

/**
 * Create the files and folders necessary for a Xedit project
 * Also it rebuild an existing project
 * 
 * @param string $project
 */
function generate_project_structure(string $project)
{
    $projectPath = project_exists($project);
    $folders = ['components', 'containers', 'layouts', 'views'];
    foreach ($folders as $folder) {
        if (!Storage::exists($projectPath . '/' . $folder)) {
            Storage::makeDirectory($projectPath . '/' . $folder);
        }
    }
}
