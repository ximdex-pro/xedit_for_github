<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class GitProjectTest extends TestCase
{
    private $url = 'https://ajlucena-ximdex@bitbucket.org/ajlucena-ximdex/project-test.git';
    private $user = 'testuser';
    private $email = 'testuser@ximdex.com';
    private $pass = 'testpass';
    private $project = 'test-project';
    private $bitbucketUser = 'ajlucena-ximdex';
    private $bitbucketToken = 'vbbZxFyLhUQTSnjDu8NQ';
    private static $token;
    private $response;
    
    /**
     * Create new test user
     */
    public function testNewUser()
    {
        DB::table('users')->where('name', $this->user)->delete();
        DB::table('users')->insert(
            [
                'name' => $this->user, 
                'email' => $this->email, 
                'password' => Hash::make($this->pass),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        $this->assertTrue(true);
    }
    
    /**
     * Log test user in order to take the API token
     */
    public function testLoginUser()
    {
        $this->response = $this->json('POST', '/api/v1/user-login', ['email' => $this->email, 'password' => $this->pass]);
        $this->check_response();
        $content = $this->response->decodeResponseJson();
        $this->assertArrayHasKey('token', $content);
        $this->assertNotEmpty($content['token']);
        self::$token = $content['token'];
    }
    
    /**
     * Clone a new project called test from a remote repository
     */
    public function testNewProject()
    {
        // $this->send_request('/api/v1/create-project/' . $this->project, 'POST');
        $this->send_request('/api/v1/clone-project/' . $this->project, 'POST', [
            'url' => $this->url,
            'user' => $this->bitbucketUser,
            'token' => $this->bitbucketToken
        ]);
        $this->check_response();
        $this->send_request('/api/v1/project-status/' . $this->project);
        $this->check_response();
        
        // Check if the readme file is present
        $this->send_request('/api/v1/list-files/' . $this->project, 'POST');
        $this->check_response();
        $content = $this->response->decodeResponseJson();
        $this->assertArrayHasKey('files', $content);
        $this->assertContains('README.md', $content['files']);
    }
    
    /**
     * Make a new folder
     */
    public function testmakeFolder()
    {
        if (!Storage::exists(PROJECTS_PATH . '/' . $this->user . '/' . $this->project . '/test')) {
            $this->send_request('/api/v1/make-folder/' . $this->project, 'POST', ['folder' => 'test']);
        } else {
            $this->send_request('/api/v1/make-folder/' . $this->project, 'POST', ['folder' => 'other-test']);
        }
        $this->check_response();
        
        // Check the folders list
        $this->send_request('/api/v1/list-folders/' . $this->project, 'POST');
        $this->check_response();
        $content = $this->response->decodeResponseJson();
        $this->assertArrayHasKey('folders', $content);
        $this->assertContains('test', $content['folders']);
    }
    
    /**
     * Create a file and edit its content
     */
    public function testEditFile()
    {
        $text = date('Y-m-d H:i:s ' . uniqid());
        $this->send_request('/api/v1/write-file/' . $this->project, 'POST', ['file' => 'test/file.txt', 'content' => $text]);
        $this->check_response();
        $this->send_request('/api/v1/read-file/' . $this->project, 'POST', ['file' => 'test/file.txt']);
        $this->check_response();
        $content = $this->response->decodeResponseJson();
        $this->assertArrayHasKey('content', $content);
        $this->assertEquals($text, $content['content']);
        
        // Check the folder test files list
        $this->send_request('/api/v1/list-files/' . $this->project, 'POST', ['folder' => 'test']);
        $this->check_response();
        $content = $this->response->decodeResponseJson();
        $this->assertArrayHasKey('files', $content);
        $this->assertContains('file.txt', $content['files']);
    }
    
    /**
     * Check if the created file is ready to be committed
     */
    public function testChanges()
    {
        $this->send_request('/api/v1/add-files/' . $this->project, 'POST');
        $this->check_response();
        $this->send_request('/api/v1/list-changes/' . $this->project);
        $this->check_response();
        $content = $this->response->decodeResponseJson();
        $this->assertArrayHasKey('files', $content);
        $this->assertContains('test/file.txt', $content['files']);
    }
    
    /**
     * Do a git commit
     */
    public function testCommit()
    {
        $this->send_request('/api/v1/do-commit/' . $this->project, 'POST', [
            'message' => 'Commit test from PHPUnit: ' . date('Y-m-d H:i:s'),
            'user' => $this->bitbucketUser,
            'email' => $this->email
        ]);
        $this->check_response();
    }
    
    /**
     * Do a git push to the remote origin
     */
    public function testPush()
    {
        $this->send_request('/api/v1/do-push/' . $this->project, 'POST', [
            'user' => $this->bitbucketUser,
            'token' => $this->bitbucketToken,
            'force' => 'true'
        ]);
        $this->check_response();
    }
    
    /**
     * Do a git pull from the remote origin
     */
    public function testPull()
    {
        $this->send_request('/api/v1/do-pull/' . $this->project, 'POST');
        $this->check_response();
    }
    
    /**
     * Delete the previous folder and file created
     */
    public function testRemoveResources()
    {
        $this->send_request('/api/v1/delete-file/' . $this->project, 'POST', ['file' => 'test/file.txt']);
        $this->check_response();
        $this->send_request('/api/v1/delete-folder/' . $this->project, 'POST', ['folder' => 'test']);
        $this->check_response();
    }
    
    /**
     * Delete project
     */
    public function testRemoveProject()
    {
        $this->send_request('/api/v1/remove-project/' . $this->project, 'POST', ['project-name' => $this->project]);
        $this->check_response();
    }
    
    /**
     * Logout user
     */
    public function testLogoutUser()
    {
        $this->send_request('/api/v1/user-logout', 'POST');
        $this->check_response();
    }
    
    /**
     * Send a request with previous generated token and retrieve the response
     * 
     * @param string $endpoint
     * @param string $method
     * @param array $params
     */
    private function send_request(string $endpoint, string $method = 'GET', array $params = [])
    {
        $this->response = $this->withHeaders(['Authorization' => 'Bearer ' . self::$token])->json(strtoupper($method), $endpoint, $params);
    }
    
    /**
     * Check if the response is a 200 code and the result contains the success value
     */
    private function check_response()
    {
        $this->response->assertOk()->assertJson(['result' => 'success']);
    }
}
